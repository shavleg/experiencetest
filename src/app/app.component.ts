import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'templatepage';

  to = true;
  loading = false;
  data = {
    experience:
      [
        [{ id: 1, title: 'Lorem Ipsum is simply', desc: 'e industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets cont', date: 'Dec. 2017 - 2019' }],
        [
          { id: 2, title: 'Lorem Ipsum is simply', desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', date: 'Dec. 2017 - 2019' },
          { id: 3, title: 'Lorem Ipsum is simply', desc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry', date: 'Dec. 2017 - 2019' },
          { id: 4, title: 'Lorem Ipsum is simply', desc: 'Lorem Ipsum is simply dummy text of the printing and', date: 'Dec. 2017 - 2019' }
        ],
        [{ id: 5, title: 'Lorem Ipsum is simply', desc: 'e industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets cont', date: 'Dec. 2017 - 2019' }]
      ]
  };

  country = [{
    "id": "1",
    "value": "Afghanistan",
    "label": "Afghanistan"
  }, {
    "id": "17",
    "value": "Albania",
    "label": "Albania"
  }, {
    "id": "18",
    "value": "Algeria",
    "label": "Algeria"
  }, {
    "id": "20",
    "value": "American Samoa",
    "label": "American Samoa"
  }, {
    "id": "22",
    "value": "Andorra",
    "label": "Andorra"
  }, {
    "id": "10",
    "value": "Angola",
    "label": "Angola"
  }, {
    "id": "11",
    "value": "Anguilla",
    "label": "Anguilla"
  }, {
    "id": "23",
    "value": "Antarctica",
    "label": "Antarctica"
  }, {
    "id": "24",
    "value": "Antigua and Barbuda",
    "label": "Antigua and Barbuda"
  }, {
    "id": "25",
    "value": "Argentina",
    "label": "Argentina"
  }, {
    "id": "26",
    "value": "Armenia",
    "label": "Armenia"
  }, {
    "id": "27",
    "value": "Aruba",
    "label": "Aruba"
  }, {
    "id": "28",
    "value": "Australia",
    "label": "Australia"
  }, {
    "id": "29",
    "value": "Austria",
    "label": "Austria"
  }, {
    "id": "12",
    "value": "Azerbaijan",
    "label": "Azerbaijan"
  }];

  get_country = [];

  ngOnInit() {
    this.get_country = this.country;
  }

  collapse(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    if (target.classList.contains('collapsed')) {
      target.classList.remove('collapsed');
    }
    else {
      target.classList.add('collapsed');
    }
  }

  autocomplate(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    target = target.parentElement;//parent of "target"
    if (target.classList.contains('collapsed')) {
      target.classList.remove('collapsed');
    }
    else {
      target.classList.add('collapsed');
    }
  }

  filter(event){
    var target = event.target || event.srcElement || event.currentTarget;
     if(target.value.length <= 0){
      let el:any = document.getElementById('country').parentElement;
      el.classList.add('collapsed');
     }
     this.get_country = this.country.filter(function (el) {
      return el.label.includes(target.value);
    });
  }

  set(value){
    (document.getElementById('country') as HTMLInputElement).value = value;
    let el:any = document.getElementById('country').parentElement;
    el.classList.remove('collapsed');
  }

  select(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    var parent = target.parentElement;//parent of "target"
    parent = parent.parentElement;
    (parent.firstChild as HTMLElement).innerHTML = target.innerText;
    parent.classList.remove('collapsed');
  }

  currentliwork(event) {
    if (event.currentTarget.checked)
      this.to = false;
    else this.to = true;
  }

  save() {
    this.loading = true;
    setTimeout(() => {
      this.loading = false;
    }, 3000)
  }
}
