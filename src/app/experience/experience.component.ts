import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.css']
})
export class ExperienceComponent implements OnInit {
  @Input() data: any = {}
  get_data: any;

  constructor() { }

  ngOnInit() {
    this.get_data = this.data.experience.slice(0, 2);
  }

  _subst(str: string) {
    return str.length <= 200 ? str : str.length > 200 ? str.substring(0, 200) : str;
  }

  show(id, event) {
    var target = event.target || event.srcElement || event.currentTarget;
    if ((document.getElementById('desc-' + id).firstChild as HTMLElement).innerHTML.length - 11 > 200) {
      for (var k in this.data.experience) {
        let d = this.data.experience[k].find(item => item.id === id);
        if (d) {
          (document.getElementById('desc-' + id).firstChild as HTMLElement).innerHTML = d.desc.substring(0, 200);
          target.innerHTML = 'More';
        }
      }

    }
    else if ((document.getElementById('desc-' + id).firstChild as HTMLElement).innerHTML.length - 11 <= 200) {
      for (var k in this.data.experience) {
        let d = this.data.experience[k].find(item => item.id === id);
        if (d) {
          (document.getElementById('desc-' + id).firstChild as HTMLElement).innerHTML = d.desc;
          target.innerHTML = 'Less';
        }
      }

    }
  }

  showmore(event) {
    var target = event.target || event.srcElement || event.currentTarget;
    if (this.get_data.length <= 2) {
      this.get_data = this.data.experience;
      target.innerHTML = "Show Less";
    }
    else {
      this.get_data = this.data.experience.slice(0, 2);
      target.innerHTML = "Show More";
    }
  }

}
